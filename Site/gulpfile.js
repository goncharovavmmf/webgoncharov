'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass');
var concatCss = require('gulp-concat-css');

gulp.task('concanate',function(){
    return gulp.src('./css/*.css')
        .pipe(concatCss('main.css'))
        .pipe(gulp.dest('./css'))
    
});
 
gulp.task('sass', function () {
  return gulp.src('./css/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});
 
gulp.task('sass:watch', function () {
  gulp.watch('./css/*.scss',['sass']);
});